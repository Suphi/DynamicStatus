### Installation
```
make install
```
dstatus is installed into /usr/local by default this can be changed by setting DESTDIR.
```
make DESTDIR=/usr install
```
***
### Usage
first we launch dstatus and tell it the delay in seconds and the path to the script it will use
```
dstatus 10 ~/scripts/myscript
```
the delay is not a simple wait for 10 seconds and then update it will sync to your system clock this means if we set the delay to 60 dstatus will update at the end of the minute on your system clock.

now we can notify dstatus with a notification message.

if you open another terminal and use the command below it will send a message to the first proccess we started telling it to show Hello World for 2 seconds.
```
dstatus n 2 "Hello World"
```
we can also notify and force dstatus to update the script after the notification has been displayed
```
dstatus u 2 "the script will update after this notification has been displayed"
```
we can also tell dstatus to use another script
```
dstatus s 60 ~/scripts/myotherscript
```
***
### Example script
```
#!/bin/bash

memory=$(awk '{OFMT = "%.0f"}NR==1{t = $2}NR==3{print (t-$2)/1024}' /proc/meminfo)
memoryPercent=$(awk '{OFMT = "%.0f"}NR==1{t = $2}NR==3{print 100*(t-$2)/t}' /proc/meminfo)
storage=$(df | awk '{OFMT = "%.1f"}$NF=="/"{print $3/1048576}')
storagePercent=$(df | awk '{OFMT = "%.0f"}$NF=="/"{print 100*$3/($3+$4)}')
temperature=$(($(</sys/class/thermal/thermal_zone4/temp)/1000))
wireless=$(awk 'NR==3 {printf "%d", ($3/70)*100}' /proc/net/wireless)
battery=$(</sys/class/power_supply/BAT0/capacity)

printf " ${memory}MB  ${storage}GB  ${temperature}C  ${wireless}%%  ${battery}%%  $(date '+%a %d %b  %H:%M:%S')"
```
More scripts can be found at [https://gitlab.com/Suphi/Scripts](https://gitlab.com/Suphi/Scripts)
***
### Removal
```
make uninstall
```
If dstatus was installed into another location you will need to set DESTDIR
```
make DESTDIR=/usr uninstall
```
