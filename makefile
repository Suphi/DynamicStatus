DESTDIR = /usr/local
MODE = 755

CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -Os -D_POSIX_C_SOURCE=199309L
LIBS = -lpthread -lX11

all: dstatus.c
	$(CC) $(CFLAGS) $(LIBS) dstatus.c -o dstatus

clean:
	rm -f dstatus

install: all
	install -d ${DESTDIR}/bin
	install -m ${MODE} dstatus ${DESTDIR}/bin

uninstall:
	rm -f ${DESTDIR}/bin/dstatus

.PHONY: all clean install uninstall
