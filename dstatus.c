#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <signal.h>
#include <sys/msg.h>
#include <X11/Xlib.h> 

#define MAXSTRING 128
#define MESSAGEID 51705

struct Message { long type; char option; int time; char text[MAXSTRING]; };

pthread_mutex_t statusMutex;
pthread_cond_t statusCond;
pthread_mutex_t notificationMutex;
pthread_cond_t notificationCond;
int exitLoop = 0;
int showingNotification = 0;
int updateScript = 0;
int statusTime = 10;
int notificationTime = 2;
char scriptPath[MAXSTRING];
char statusString[MAXSTRING];
char notificationString[MAXSTRING];

void fatalsig(int signum) {
	exitLoop = 1;
}

void sigusr1(int signum) {

}

void print(char *string) {
	Display *dpy = XOpenDisplay(NULL);
	Window root = RootWindow(dpy, DefaultScreen(dpy));
	XStoreName(dpy, root, string);
	XCloseDisplay(dpy);
}

void readScript() {
	updateScript = 0;
	FILE *file;
	file = popen(scriptPath, "r");
	if (file == NULL) return;
	fgets(statusString, MAXSTRING, file);
	pclose(file);
}

void *statusThread(void *arg) {
	pthread_mutex_lock(&statusMutex);
	struct timespec timeSpec;
	while (!exitLoop) {
		readScript();	
		if (!showingNotification) print(statusString);
		clock_gettime(CLOCK_REALTIME, &timeSpec);
		timeSpec.tv_sec += (statusTime  - (timeSpec.tv_sec - (timeSpec.tv_sec / statusTime) * statusTime));
		timeSpec.tv_nsec = 0;
		pthread_cond_timedwait(&statusCond, &statusMutex, &timeSpec);
	}
	pthread_mutex_unlock(&statusMutex);
	return 0;
}

void *notificationThread(void *arg) {
	pthread_mutex_lock(&notificationMutex);
	while (!exitLoop) {
		pthread_cond_wait(&notificationCond, &notificationMutex);
		if (exitLoop) break;
		showingNotification = 1;
		while (!exitLoop) {
			print(notificationString);
			struct timespec timeSpec;
			clock_gettime(CLOCK_REALTIME, &timeSpec);
			timeSpec.tv_sec += notificationTime;
			if (pthread_cond_timedwait(&notificationCond, &notificationMutex, &timeSpec) == ETIMEDOUT) break;
		}
		showingNotification = 0;
		if (updateScript) readScript();
		print(statusString);
	}
	pthread_mutex_unlock(&notificationMutex);
	return 0;
}

void sendMessage(char option, int time, char *text) {
	int messageID = msgget(MESSAGEID, 0660);
	if(messageID < 0) return;
	
	struct Message message;
	message.type = 1;
	message.option = option;
	message.time = time;
	strcpy(message.text, text);
	
	msgsnd(messageID, &message, sizeof(struct Message) - sizeof(long), IPC_NOWAIT);
}

void mainLoop() {
	struct Message message;
	int messageID = msgget(MESSAGEID, IPC_CREAT | 0660);
	if(messageID < 0) return;
	
	struct sigaction action1;
	action1.sa_handler = fatalsig;
	sigaction(SIGPIPE, &action1, NULL);
	sigaction(SIGTERM, &action1, NULL);
	sigaction(SIGINT, &action1, NULL);

	struct sigaction action2;
	action2.sa_handler = sigusr1;
	sigaction(SIGUSR1, &action2, NULL);

	pthread_mutex_init(&notificationMutex, NULL);
	pthread_cond_init(&notificationCond, NULL);
	pthread_mutex_init(&statusMutex, NULL);
	pthread_cond_init(&statusCond, NULL);
	pthread_t threadNotification;
	pthread_create(&threadNotification, NULL, notificationThread, NULL);
	pthread_t threadStatus;
	pthread_create(&threadStatus, NULL, statusThread, NULL);

	while (!exitLoop) {
		if (msgrcv(messageID, &message, sizeof(struct Message) - sizeof(long), 1, 0) < 0) break;
		switch (message.option) {
		case 'n':
			notificationTime = message.time;
			strcpy(notificationString, message.text);
			pthread_cond_signal(&notificationCond);
			break;
		case 'u':
			updateScript = 1;
			notificationTime = message.time;
			strcpy(notificationString, message.text);
			pthread_cond_signal(&notificationCond);
			break;
		case 's':
			statusTime = message.time;
			strcpy(scriptPath, message.text);
			pthread_cond_signal(&statusCond);
			break;
		}
	}
	
	msgctl(messageID, IPC_RMID, (struct msqid_ds*)0);
	
	pthread_cond_signal(&notificationCond);
	pthread_cond_signal(&statusCond);
	pthread_join(threadNotification, NULL);
	pthread_join(threadStatus, NULL);
	pthread_cond_destroy(&notificationCond);
	pthread_mutex_destroy(&notificationMutex);
	pthread_cond_destroy(&statusCond);
	pthread_mutex_destroy(&statusMutex);
}

int main(int argc, char *argv[]) {
	if (argc == 4) {
		sendMessage(argv[1][0], atoi(argv[2]), argv[3]);
		return 0;
	}
	if (argc == 3) {
		statusTime = atoi(argv[1]);
		strcpy(scriptPath, argv[2]);
		mainLoop();
		return 0;
	}
	printf("%s\n%-4s%s %-25s%s\n%-4s%s %-25s%s\n%-4s%s %-25s%s\n%-4s%s %-25s%s\n",
		"usage:",
		"", argv[0], "[delay] [path]", "#launch with script",
		"", argv[0], "s [delay] [path]", "#set new script",
		"", argv[0], "n [delay] [string]", "#notify",
		"", argv[0], "u [delay] [string]", "#notify then update");
	return 0;
}
